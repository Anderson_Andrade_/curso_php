<?php
//Ponteiro é a barra de digitação do computador
//w+ move o ponteiro para o inicio do arquivo
//a+ move o ponteiro para o final do arquivo

$file = fopen('log.txt', 'a+');

fwrite($file,date('m - d - Y H:i:s' . "\r\n"));

fclose($file);

echo 'Criado com sucesso';
