<?php
class Sql extends PDO{
    private $conn;

    function __contruct(){
        $this->conn = new PDO('mysql:host=localhost;dbname=php','root','');
    }

    function setParams($statment, $parameters=array()){
        foreach ($parameters as $key => $value) {
            $statment->bindParam($statment, $key, $value);
        }
    }

    function setParam($statment, $key, $value){
        $statment->bindParam($key, $value);
    }

    function query($rawQuery, $params = array()){
        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();

        return $stmt;
    }

    function selection($rawQuery, $param = array()):array {
        $stmt = $this->query($rawQuery, $param);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}