<?php
class Usuario{
    private $id;
    private $nome;
    private $senha;
    private $email;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    function loadById($id){
        $sql = new Sql();

        $results->select('select * from tb_usuarios where ID = :ID', array(':ID'=>$id));

        if (count($results)>0){
            $this->setData($results[0]);
        }
    }

    function __toString(){
        return json_encode(array(
            'ID'=>$this->getId(),
            'NOME'=>$this->getNome(),
            'EMAIL'=>$this->getEmail(),
            'SENHA'=>$this->getSenha()
            ));
    }

    static function getList(){
        $sql = new Sql();

        return $sql->selection('select * from tb_usuarios');
    }

    function login($login, $password){
        $sql = new Sql();

        $results = $sql->select('select * from  tb_usuarios where NOME = :NOME and  SENHA = :SENHA', array(
            ':NOME'=>$login,
            ':SENHA'=>$password
        ));

        if (count($results)>0){

            $row = $results[0];

            $this->setData($results[0]);
        }

    }

    function setData($data){
        $this->setId($data['ID']);
        $this->setNome($data['NOME']);
        $this->setEmail($data['EMAIL']);
        $this->setSenha($data['SENHA']);
    }

    function insert(){
        $sql = new Sql();
        $resuts = $sql->select('CALL sp_usuarios_insert(:NOME, :SENHA)', array());
            ':NOME'=> $this->getNome();
            ':SENHA'=>$this->getSenha();

            if (count($resuts) > 0){
                $this->setData($resuts[0]);
            }

    }

    function update($login, $password){
        $sql = new Sql();

        $this->setNome($login);
        $this->setSenha($password);

        $sql->query('update tb_usuarios set NOME = :NOME, SENHA = :SENHA where ID = :ID', array(
            ':NOME'=>$this->getNome(),
            ':SENHA'=>$this->getSenha(),
            ':ID'=>$this->getId()
        ));
    }

    function delete(){
        $sql = new Sql();

        $sql->query('delete from tb_usuarios where ID = :ID', array(
            ':ID'=>$this->getId()
        ));

        $this->setId(0);
        $this->setNome('');
        $this->setEmail('');
        $this->setSenha('');
    }
}