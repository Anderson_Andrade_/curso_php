<?php

$conn = new mysqli('localhost','root','','php');

if($conn->connect_error){
    echo "Error: " . $conn->connect_error;
}

$result = $conn->query('select * from tb_usuarios');

$json = array();

while ($row = $result->fetch_assoc()){
    array_push($json, $row);
}

echo json_encode($json);