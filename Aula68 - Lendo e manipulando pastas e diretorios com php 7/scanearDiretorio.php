<?php
$folder = scandir('imagens');

$data  = array();

foreach ($folder as $img){
    if (!in_array($img, array('.','..'))){
        $filename = 'imagens'. DIRECTORY_SEPARATOR . $img;

        $info = pathinfo($filename);

        $info['size'] = filesize($filename);
        $info['modified'] = date('d/m/Y', filemtime($filename));
        $info['url'] =
            'http:'.DIRECTORY_SEPARATOR.'localhost'.DIRECTORY_SEPARATOR.'curso_php'.DIRECTORY_SEPARATOR.'Aula68%20-%20Lendo%20e%20manipulando%20pastas%20e%20diretorios%20com%20php%207'.DIRECTORY_SEPARATOR.$filename.'.php';

        array_push($data, $info);
    };
}
echo json_encode($data);